grammar BeNice;

start:              START (stat)* END;

stat:               START_STAT statement END_STAT;

statement:          print_statement |
                    read_statement |
                    assign_statement;

print_statement:    PRINT value;

read_statement:     READ ID;

assign_statement:   ASSIGN operation TO ID;

operation:          operation sign operation |
                    BR_OP operation BR_CL |
                    value;

value:              ID |
                    STRING |
                    INT |
                    REAL;
//                    array;

sign:               ADD |
                    SUB |
                    MULT |
                    DIV;

//array:              '[' value (',' value)* ']';

START:              'dear_computer';
END:                'thank_you';
START_STAT:         'could_you';
END_STAT:           'please';

PRINT:              'print';
READ:               'read';
ASSIGN:             'assign';
TO:                 'to';

ID:                 ('a'..'z'|'A'..'Z')+;
STRING:             '"' ( ~('\\'|'"') )* '"';
INT:                '-'?'0'..'9'+;
REAL:               '-'?'0'..'9'+('.''0'..'9'+)?;

ADD:                '+';
SUB:               '-';
MULT:               '*';
DIV:                '/';

BR_OP:              '(';
BR_CL:              ')';
WS :                [ \t\r\n]+ -> skip;