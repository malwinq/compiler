// Generated from BeNice.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link BeNiceParser}.
 */
public interface BeNiceListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link BeNiceParser#start}.
	 * @param ctx the parse tree
	 */
	void enterStart(BeNiceParser.StartContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeNiceParser#start}.
	 * @param ctx the parse tree
	 */
	void exitStart(BeNiceParser.StartContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeNiceParser#stat}.
	 * @param ctx the parse tree
	 */
	void enterStat(BeNiceParser.StatContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeNiceParser#stat}.
	 * @param ctx the parse tree
	 */
	void exitStat(BeNiceParser.StatContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeNiceParser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(BeNiceParser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeNiceParser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(BeNiceParser.StatementContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeNiceParser#print_statement}.
	 * @param ctx the parse tree
	 */
	void enterPrint_statement(BeNiceParser.Print_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeNiceParser#print_statement}.
	 * @param ctx the parse tree
	 */
	void exitPrint_statement(BeNiceParser.Print_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeNiceParser#read_statement}.
	 * @param ctx the parse tree
	 */
	void enterRead_statement(BeNiceParser.Read_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeNiceParser#read_statement}.
	 * @param ctx the parse tree
	 */
	void exitRead_statement(BeNiceParser.Read_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeNiceParser#assign_statement}.
	 * @param ctx the parse tree
	 */
	void enterAssign_statement(BeNiceParser.Assign_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeNiceParser#assign_statement}.
	 * @param ctx the parse tree
	 */
	void exitAssign_statement(BeNiceParser.Assign_statementContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeNiceParser#operation}.
	 * @param ctx the parse tree
	 */
	void enterOperation(BeNiceParser.OperationContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeNiceParser#operation}.
	 * @param ctx the parse tree
	 */
	void exitOperation(BeNiceParser.OperationContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeNiceParser#value}.
	 * @param ctx the parse tree
	 */
	void enterValue(BeNiceParser.ValueContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeNiceParser#value}.
	 * @param ctx the parse tree
	 */
	void exitValue(BeNiceParser.ValueContext ctx);
	/**
	 * Enter a parse tree produced by {@link BeNiceParser#sign}.
	 * @param ctx the parse tree
	 */
	void enterSign(BeNiceParser.SignContext ctx);
	/**
	 * Exit a parse tree produced by {@link BeNiceParser#sign}.
	 * @param ctx the parse tree
	 */
	void exitSign(BeNiceParser.SignContext ctx);
}