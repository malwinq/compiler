import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import java.io.*;

public class Main {
    public static void main(String[] args) throws Exception {

        // params
        String filename_input = "D:\\repo\\compiler_local\\BeNice2\\test.nice";
        String filename_output = "D:\\repo\\compiler_local\\BeNice2\\test.ll";
        PrintStream console = System.out;

        // base code
        System.out.println("\nBASE CODE:");
        try (BufferedReader br = new BufferedReader(new FileReader(filename_input))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            } }

        // antlr
        ANTLRFileStream input = new ANTLRFileStream(filename_input);
        BeNiceLexer lexer = new BeNiceLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        BeNiceParser parser = new BeNiceParser(tokens);
        ParseTree tree = parser.start();
        System.out.println("\nANTLR OUTPUT: ");
        System.out.println(tree.toStringTree(parser));

        // parsing to IR LLVM
        System.out.println("\nLLVM INPUT: ");
        System.setOut(new ProxyPrintStream(System.out, filename_output));
        ParseTreeWalker walker = new ParseTreeWalker();
        walker.walk(new LLVMActions(), tree);

        // ANTLR errors
        System.setOut(console);
        ANTLRFileStream input_err = new ANTLRFileStream(filename_input);
        BeNiceLexer lexer_er = new BeNiceLexer(input_err);
        CommonTokenStream tokens_er = new CommonTokenStream(lexer_er);
        BeNiceParser parser_er = new BeNiceParser(tokens_er);
        ParseTree tree_er = parser_er.start();
    }
}
