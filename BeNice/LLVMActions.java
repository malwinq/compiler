import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LLVMActions extends BeNiceBaseListener {

    HashMap<String, String> memory = new HashMap<String, String>();
    String value = "0";
    String prev_value;
    HashMap<String, Boolean> calc = new HashMap<String, Boolean>() {{
        put("Add", false);
        put("Sub", false);
        put("Mult", false);
        put("Div", false);
    }};
    List<String> array = new ArrayList<String>();
    boolean if_array = false;
    String array_str;

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";

    @Override
    public void exitStart(BeNiceParser.StartContext ctx) {
        System.out.println( LLVMGenerator.generate() );
    }

    @Override
    public void exitValue(BeNiceParser.ValueContext ctx) {
        prev_value = value;
        if( ctx.ID() != null ) {
            value = memory.get(ctx.ID().getText());
            if( value == null ){
                System.out.println(ANSI_RED + "ERROR: Element not found\n" + ANSI_RESET);
                System.exit(1);
            }
        }
        if( ctx.INT() != null ) {
            value = ctx.INT().getText();
        }
        if( ctx.REAL() != null ) {
            value = ctx.REAL().getText();
        }
        if( ctx.STRING() != null ) {
            value = ctx.STRING().getText();
            value = value.substring(1, value.length() - 1);
        }
        if( ctx.ITEM() != null ) {
            String item = ctx.ITEM().getText();
            String[] elements = item.split("<");
            String id_arr = elements[0];
            int id_el = Integer.parseInt(elements[1].split(">")[0]);
            value = memory.get(id_arr);
            value = value.substring(1, value.length() - 1);
            String[] items = value.split(", ");
            value = items[id_el];
        }

        if( if_array ) {
            array.add(value);
        }
        if( ctx.array() != null) {
            value = array_str;
        }
    }

    @Override
    public void exitAssign_statement(BeNiceParser.Assign_statementContext ctx) {
        String id = ctx.ID().getText();
        memory.put(id, value);
    }

    @Override
    public void exitOperation(BeNiceParser.OperationContext ctx) {
        if (calc.get("Add") || calc.get("Sub") || calc.get("Mult") || calc.get("Div")) {
            double prev_val;
            try {
                prev_val = Double.parseDouble(prev_value);
            } catch (Exception e) {
                prev_val = 0;
            }
            double val;
            try {
                val = Double.parseDouble(value);
                if (calc.get("Add")) {
                    val = prev_val + val;
                    calc.put("Add", false);
                }
                if (calc.get("Sub")) {
                    val = prev_val - val;
                    calc.put("Sub", false);
                }
                if (calc.get("Mult")) {
                    val = prev_val * val;
                    calc.put("Mult", false);
                }
                if (calc.get("Div")) {
                    if (val == Double.parseDouble("0")) {
                        System.out.println(ANSI_RED + "ERROR: Division by 0\n" + ANSI_RESET);
                        System.exit(1);
                    }
                    val = prev_val / val;
                    calc.put("Div", false);
                }
                value = String.valueOf(val);

            } catch (Exception e) {
                System.out.println(e.getMessage());
                System.out.println(ANSI_RED + "ERROR: Operation cannot be done with strings and arrays\n" + ANSI_RESET);
                System.exit(1);
            }
        }
    }

    @Override
    public void exitSign(BeNiceParser.SignContext ctx) {
        if( ctx.ADD() != null ) {
            calc.put("Add", true);
        }
        if( ctx.SUB() != null ) {
            calc.put("Sub", true);
        }
        if( ctx.MULT() != null ) {
            calc.put("Mult", true);
        }
        if( ctx.DIV() != null ) {
            calc.put("Div", true);
        }
    }

    @Override
    public void enterArray(BeNiceParser.ArrayContext ctx) {
        array.clear();
        if_array = true;
    }

    @Override
    public void exitArray(BeNiceParser.ArrayContext ctx) {
        array_str = String.join(", ", array);
        array_str = '[' + array_str + ']';
    }

    @Override
    public void exitRead_statement(BeNiceParser.Read_statementContext ctx) {
        LLVMGenerator.read(ctx.ID().getText());
    }

    @Override
    public void exitPrint_statement(BeNiceParser.Print_statementContext ctx) {
        LLVMGenerator.print(value);
    }
}
