declare i32 @printf(i8*, ...)
declare i32 @getchar()
@text = global [14 x i8] c"You entered: \00"
@str0 = constant[5 x i8] c"8.0\0A\00"
@str1 = constant[6 x i8] c"80.0\0A\00"
@str2 = constant[8 x i8] c"lalala\0A\00"
@str3 = constant[6 x i8] c"10.0\0A\00"
@str4 = constant[8 x i8] c"blabla\0A\00"
@str5 = constant[5 x i8] c"8.0\0A\00"
define i32 @main() nounwind{
%1 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ( [5 x i8], [5 x i8]* @str0, i32 0, i32 0))
%2 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ( [6 x i8], [6 x i8]* @str1, i32 0, i32 0))
%3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ( [8 x i8], [8 x i8]* @str2, i32 0, i32 0))
%4 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ( [6 x i8], [6 x i8]* @str3, i32 0, i32 0))
%5 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ( [8 x i8], [8 x i8]* @str4, i32 0, i32 0))
%6 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ( [5 x i8], [5 x i8]* @str5, i32 0, i32 0))
ret i32 0 }

