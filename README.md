# The compiler of language BeNice

BeNice is my own programming language. The code has to start with "dear_computer"
and end with "thank_you". Every instruction start with "could_you" and ends with 
"please". 

The lexical and syntax analysis is made by ANTLR 4.8 on Java. It generates the
AST tree. It is parsed by my code (in Java) to intermediate representation (IR).
IR is executed by LLVM.