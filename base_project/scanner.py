import collections
import re
from prettytable import PrettyTable

""" Lexical analysis """

Token = collections.namedtuple('Token', ['type', 'value', 'line', 'column'])


class Scanner:
    """ Class for scanner - the lexical analysis"""

    def __init__(self, input_string):
        self.table = PrettyTable()
        self.table.field_names = ['Type', 'Value', 'Line', 'Column']
        self.tokens = self.tokenize(input_string)
        self.current_token_number = 0

    def next_token(self):
        self.current_token_number += 1
        if self.current_token_number - 1 < len(self.tokens):
            return self.tokens[self.current_token_number - 1]
        else:
            raise RuntimeError('Error: No more tokens')

    def print_tokens(self):
        print('\n========================= AFTER LEXICAL ANALYSIS ==========================\n')
        print(self.table)

    def tokenize(self, input):
        """ Splitting code into keywords """

        keywords = ['$id', '$schema', 'title', 'type', 'properties', 'description', 'required', 'minimum',
                    'maximum', 'minLength', 'maxLength', 'enum', 'definitions', '$ref']
        keywords[:] = ['"' + item + '"' for item in keywords]

        token_specification = [
            ('STRING', r'\"(.*?)\"'),
            ('ASSIGN', r':'),
            ('END', r','),
            ('CURLY_OP', r'\{'),
            ('CURLY_CL', r'\}'),
            ('SQUARE_OP', r'\['),
            ('SQUARE_CL', r'\]'),
            ('NUMBER', r'\d+(\.\d*)?'),
            ('ARITH_OP', r'[+*\/\-]'),
            ('NEWLINE', r'\n'),
            ('SPACE', r'[ \t]')]

        tok_regex = '|'.join('(?P<%s>%s)' % pair for pair in token_specification)

        get_token = re.compile(tok_regex).match

        match = get_token(input)

        tokens = []
        line_number = 1
        line_start = 0
        position = 0

        while match is not None:
            type = match.lastgroup

            if type == 'NEWLINE':
                line_start = position
                line_number += 1
            elif type != 'SPACE':
                value = match.group(type)
                if type == 'STRING' and value in keywords:
                    type = value

                next_token = Token(type, value, line_number, match.start() - line_start)
                tokens.append(next_token)
                self.table.add_row(next_token)

            position = match.end()
            match = get_token(input, position)

        if position != len(input):
            raise RuntimeError('Error: Unexpected character %r on line %d' % (input[position], line_number))

        next_token = Token('EOF', '', line_number, position - line_start)
        tokens.append(next_token)

        return tokens
