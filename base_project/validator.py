from scanner import *
from parser import *

""" Validate the example code """

path = r'/home/malwina/repo/compiler/base_project/exampleFile.txt'

with open(path) as file:
    input_string = file.read()

print('\n========================= BASE CODE ==========================\n')
print(input_string)

# lexical analysis
scanner = Scanner(input_string)
scanner.print_tokens()

# syntax analysis
parser = Parser(scanner)
parser.start()
parser.print_tokens()
print('\n\t\t PARSING SUCCESSFUL\n')
