from prettytable import PrettyTable

""" Syntax analysis """


class Parser:
    """     BNF
    start -> CURLY_OP statement program CURLY_CL EOF

    program -> END statement program
    program -> eps

    statement -> id_stmt
    statement -> schema_stmt
    statement -> title_stmt
    statement -> type_stmt
    statement -> description_stmt
    statement -> ref_stmt
    statement -> string_stmt
    statement -> enum_stmt
    statement -> required_stmt
    statement -> properties_stmt
    statement -> definitions_stmt
    statement -> min_stmt
    statement -> max_stmt
    statement -> minL_stmt
    statement -> maxL_stmt

    id_stmt -> "$id" assign_string
    schema_stmt -> "$schema" assign_string
    title_stmt -> "title" assign_string
    type_stmt -> "type" assign_string
    description_stmt -> "description" assign_string
    ref_stmt -> "$ref" assign_string
    min_stmt -> "minimum" assign_number
    max_stmt -> "maximum" assign_number
    minL_stmt -> "minLength" assign_number
    maxL_stmt -> "maxLength" assign_number
    enum_stmt -> "enum" assign_list
    required_stmt -> "required" assign_list
    properties_stmt -> "properties" assign_dict
    definitions_stmt -> "definitions" assign_dict
    string_stmt -> STRING ASSIGN value

    assign_string -> ASSIGN STRING
    assign_number -> ASSIGN NUMBER
    assign_list -> ASSIGN list
    assign_dict -> ASSIGN dictionary
    value -> STRING
    value -> dictionary

    list -> SQUARE_OP STRING list_elements SQUARE_CL
    list_elements -> END STRING list_elements
    list_elements -> eps

    dictionary -> CURLY_OP statement dict_elements CURLY_CL
    dict_elements -> END statement dict_elements
    dict_elements -> eps
    """

    def __init__(self, scanner):
        self.next_token = scanner.next_token
        self.token = self.next_token()

        self.keywords = ['$id', '$schema', 'title', 'type', 'properties', 'description', 'required', 'minimum',
                         'maximum', 'minLength', 'maxLength', 'enum', 'definitions', '$ref']
        self.keywords[:] = ['"' + item + '"' for item in self.keywords]
        self.keywords.append('STRING')

        self.assign_keywords = ['$id', '$schema', 'title', 'type', 'description', '$ref']
        self.assign_keywords[:] = ['"' + item + '"' for item in self.assign_keywords]
        self.assign_keywords.append('STRING')

        self.num_keywords = ['minimum', 'maximum', 'minLength', 'maxLength']
        self.num_keywords[:] = ['"' + item + '"' for item in self.num_keywords]

        self.table = PrettyTable()
        self.table.field_names = ['Line', 'Type of statement', 'Status']

    def error(self, msg):
        raise RuntimeError('Error while parsing: ', msg)

    def take_token(self, token_type):
        if self.token.type != token_type:
            self.table.add_row([self.token.line, self.token.type, 'ERROR'])
            self.print_tokens()
            print('Error in take_token function, got:', self.token.type, ', expected:', token_type, '\n\n\n')
            self.error('Unexpected token: %s' % token_type)

        if token_type != 'EOF':
            self.token = self.next_token()

    def info(self, stmt_name):
        self.table.add_row([self.token.line, stmt_name, 'OK'])

    def print_tokens(self):
        print('\n\n======= AFTER SYNTAX ANALYSIS =======\n')
        print(self.table)

    # START --------------------------------------------------

    def start(self):
        # start -> CURLY_OP statement program CURLY_CL EOF
        self.take_token('CURLY_OP')
        if self.token.type in self.keywords:
            self.statement()
            self.program()
            self.take_token('CURLY_CL')
            self.take_token('EOF')

        else:
            self.error("Epsilon not allowed")

    def program(self):
        # program -> END statement program
        if self.token.type == 'END':
            self.take_token('END')
            self.statement()
            self.program()

        # program -> eps
        else:
            pass

    def statement(self):
        # statement -> id_stmt
        if self.token.type == '"$id"':
            self.id_stmt()

        # statement -> schema_stmt
        elif self.token.type == '"$schema"':
            self.schema_stmt()

        # statement -> title_stmt
        elif self.token.type == '"title"':
            self.title_stmt()

        # statement -> type_stmt
        elif self.token.type == '"type"':
            self.type_stmt()

        # statement -> description_stmt
        elif self.token.type == '"description"':
            self.description_stmt()

        # statement -> ref_stmt
        elif self.token.type == '"$ref"':
            self.ref_stmt()

        # statement -> string_stmt
        elif self.token.type == 'STRING':
            self.string_stmt()

        # statement -> enum_stmt
        elif self.token.type == '"enum"':
            self.enum_stmt()

        # statement -> required_stmt
        elif self.token.type == '"required"':
            self.required_stmt()

        # statement -> properties_stmt
        elif self.token.type == '"properties"':
            self.properties_stmt()

        # statement -> definitions_stmt
        elif self.token.type == '"definitions"':
            self.definitions_stmt()

        # statement -> min_stmt
        elif self.token.type == '"minimum"':
            self.min_stmt()

        # statement -> max_stmt
        elif self.token.type == '"maximum"':
            self.max_stmt()

        # statement -> minL_stmt
        elif self.token.type == '"minLength"':
            self.minL_stmt()

        # statement -> maxL_stmt
        elif self.token.type == '"maxLength"':
            self.maxL_stmt()

        else:
            self.error("Epsilon not allowed")

    # STATEMENTS ------------------------------------------------------------

    def id_stmt(self):
        # id_stmt -> "$id" assign_string
        self.take_token('"$id"')
        self.assign_string()
        self.info('ID')

    def schema_stmt(self):
        # schema_stmt -> "$schema" assign_string
        self.take_token('"$schema"')
        self.assign_string()
        self.info('Schema')

    def title_stmt(self):
        # title_stmt -> "title" assign_string
        self.take_token('"title"')
        self.assign_string()
        self.info('Title')

    def type_stmt(self):
        # type_stmt -> "type" assign_string
        self.take_token('"type"')
        self.assign_string()
        self.info('Type')

    def description_stmt(self):
        # description_stmt -> "description" assign_string
        self.take_token('"description"')
        self.assign_string()
        self.info('Description')

    def ref_stmt(self):
        # ref_stmt -> "$ref" assign_string
        self.take_token('"$ref"')
        self.assign_string()
        self.info('Ref')

    def min_stmt(self):
        # min_stmt -> "minimum" assign_number
        self.take_token('"minimum"')
        self.assign_number()
        self.info('Minimum')

    def max_stmt(self):
        # max_stmt -> "maximum" assign_number
        self.take_token('"maximum"')
        self.assign_number()
        self.info('Maximum')

    def minL_stmt(self):
        # minL_stmt -> "minLength" assign_number
        self.take_token('"minLength"')
        self.assign_number()
        self.info('MinLength')

    def maxL_stmt(self):
        # maxL_stmt -> "maxLength" assign_number
        self.take_token('"maxLength"')
        self.assign_number()
        self.info('MaxLength')

    def enum_stmt(self):
        # enum_stmt -> "enum" assign_list
        self.take_token('"enum"')
        self.assign_list()
        self.info('Enum')

    def required_stmt(self):
        # required_stmt -> "required" assign_list
        self.take_token('"required"')
        self.assign_list()
        self.info('Required')

    def properties_stmt(self):
        # properties_stmt -> "properties" assign_dict
        self.take_token('"properties"')
        self.assign_dict()
        self.info('Properties')

    def definitions_stmt(self):
        # definitions_stmt -> "definitions" assign_dict
        self.take_token('"definitions"')
        self.assign_dict()
        self.info('Definitions')

    def string_stmt(self):
        # string_stmt -> STRING ASSIGN value
        self.take_token('STRING')
        self.take_token('ASSIGN')
        self.value()
        self.info('String')

    def assign_string(self):
        # assign_string -> ASSIGN STRING
        self.take_token('ASSIGN')
        self.take_token('STRING')

    def assign_number(self):
        # assign_number -> ASSIGN NUMBER
        self.take_token('ASSIGN')
        self.take_token('NUMBER')

    def assign_list(self):
        # assign_list -> ASSIGN list
        self.take_token('ASSIGN')
        self.list()

    def assign_dict(self):
        # assign_dict -> ASSIGN dictionary
        self.take_token('ASSIGN')
        self.dictionary()

    def value(self):
        # value -> STRING
        if self.token.type == 'STRING':
            self.take_token('STRING')

        # value -> dictionary
        elif self.token.type == 'CURLY_OP':
            self.dictionary()

        else:
            self.error("Epsilon not allowed")

    # LIST --------------------------------------------------------------

    def list(self):
        # list -> SQUARE_OP STRING list_elements SQUARE_CL
        self.take_token('SQUARE_OP')
        self.take_token('STRING')
        self.list_elements()
        self.take_token('SQUARE_CL')

    def list_elements(self):
        # list_elements -> END STRING list_elements
        if self.token.type == 'END':
            self.take_token('END')
            self.take_token('STRING')
            self.list_elements()

        # list_elements -> eps
        else:
            pass

    # DICT --------------------------------------------------------------

    def dictionary(self):
        # dictionary -> CURLY_OP statement dict_elements CURLY_CL
        self.take_token('CURLY_OP')
        self.statement()
        self.dict_elements()
        self.take_token('CURLY_CL')

    def dict_elements(self):
        # dict_elements -> END statement dict_elements
        if self.token.type == 'END':
            self.take_token('END')
            self.statement()
            self.dict_elements()

        # dict_elements -> eps
        else:
            pass
